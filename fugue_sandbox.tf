terraform {
  required_providers {
    fugue = {
      version = "0.0.1"
      source  = "fugue/fugue"
    }
  }
}

provider "fugue" {}

data "fugue_aws_types" "all" {
  region = "us-east-1"
}

resource "fugue_aws_environment" "Sandbox" {
  name = "Sandbox"
  role_arn = "arn:aws:iam::021358264478:role/Fugue1609277904"
  regions = ["*"]
  compliance_families = ["FBP", "CIS"]
  resource_types = data.fugue_aws_types.all.types
}

output "aws_env_id" {
  value = fugue_aws_environment.Sandbox.id
}
